To make this project work install all requirements using 'pip' in your virtual environment and change database settings in gallerysite/settings.py
Then run 'python manage.py migrate' to create database tables.
After that you should create a superuser using command 'python manage.py createsuperuser'.
