from django.shortcuts import render
from django.http import HttpResponse
from .models import Category, Image
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404

class Categorylist(ListView):
    template_name = 'gallery/index.html'
    context_object_name = 'list_categories'
    queryset = Category.objects.order_by('-name')

class ResultList(ListView):
    template_name = 'gallery/results.html'

    def get_queryset(self):
        category = get_object_or_404(Category, id=self.args[0])
        return Image.objects.filter(category = category)

class ImageDetail(DetailView):
    model = Image
    template_name = 'gallery/detail.html'
    context_object_name = 'image_detail'
    def get_context_data(self, **kwargs):
        context = super(ImageDetail, self).get_context_data(**kwargs)
        return context