from django.contrib import admin
from .models import Image, Category

# Register your models here.
@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('item', 'name', 'category', 'publication_date')
    list_filter = ('name', 'category', 'publication_date')
    search_fields = ('category',)
admin.site.register(Category)
