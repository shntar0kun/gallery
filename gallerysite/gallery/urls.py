from django.conf.urls import url
from . import views
from .views import Categorylist, ResultList, ImageDetail
from django.conf import settings
from django.conf.urls.static import static

app_name = 'GALLERY'
urlpatterns = [
    url(r'^$', Categorylist.as_view()),
    url(r'^([\d]+)/$', ResultList.as_view()),
    url(r'^(?P<category_id>[0-9]+)/(?P<pk>[0-9]+)/$', ImageDetail.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)