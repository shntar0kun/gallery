from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Image(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    item = models.ImageField(upload_to = 'gallery')
    publication_date = models.DateTimeField('Publication Date')

    def __str__(self):
        return self.name
