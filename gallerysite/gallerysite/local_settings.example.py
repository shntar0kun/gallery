DATABASES = {
    'default': {
        'NAME': 'your_db_name',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'your_db_user',
        'PASSWORD': 'your_db_password',
        'OPTIONS': {
          'autocommit': True,
        },
    }
}